//
//  ViewController.swift
//  20230718_MessengerXib
//
//  Created by Yen Lin on 2023/7/18.
//

import UIKit

class MessengerViewController: UIViewController {
    
    var messages = [Message]() {
        didSet {
            Message.saveMessage(messages)
        }
    }
    
    
    var tableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.allowsMultipleSelection = false
        return tableView
    }()
    
    var inputStackView: UIStackView = {
       let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.distribution = .fill
        stackView.spacing = 16
        stackView.backgroundColor = .black
        stackView.layoutMargins = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    var inputTextField: UITextField = {
        let inputTextField = UITextField()
        inputTextField.backgroundColor = .white
        inputTextField.placeholder = "Enter a message"
        inputTextField.layer.cornerRadius = 18
        inputTextField.clipsToBounds = true
       return inputTextField
    }()
    
    var sendButton: UIButton = {
       let button = UIButton()
        button.setImage(UIImage(systemName: "paperplane.fill"), for: .normal)
        button.tintColor = .link
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        navigationItem.title = "Messenger"
        
        messages = [
            Message(sender: "A-Lin", messageContent: "雨都停了"),
            Message(sender: "A-Lin", messageContent: "這片天，灰什麼呢？"),
            Message(sender: "A-Lin", messageContent: "(換你唱～)")
        ]
        
        if let messages = Message.loadMessage() {
            self.messages = messages
        }

        tableView.dataSource = self
        tableView.delegate = self
        
        let cellXib = UINib(nibName: "\(SenderTableViewCell.self)", bundle: nil)
        tableView.register(cellXib, forCellReuseIdentifier: "\(SenderTableViewCell.self)")
        tableView.register(ReceiverTableViewCell.self, forCellReuseIdentifier: "\(ReceiverTableViewCell.self)")
        
        sendButton.addTarget(self, action: #selector(sendButtonPressed), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)

        inputTextField.delegate = self
        
    }
    
    @objc func sendButtonPressed() {
        if let inputMessage = inputTextField.text,
           inputMessage != "" {
            messages.append(Message(sender: "me", messageContent: inputMessage))
            print(messages)
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        inputStackView.addArrangedSubview(inputTextField)
        inputTextField.translatesAutoresizingMaskIntoConstraints = false
        inputTextField.heightAnchor.constraint(greaterThanOrEqualToConstant: 36).isActive = true
        
        //避免輸入框上的光標被圓角吃掉嗚嗚
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: inputTextField.frame.height))
        inputTextField.leftView = paddingView
        inputTextField.leftViewMode = .always
        
        inputStackView.addArrangedSubview(sendButton)
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            sendButton.trailingAnchor.constraint(equalTo: inputStackView.trailingAnchor, constant: -16)
        ])
        
        
        view.addSubview(inputStackView)
        inputStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            inputStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            inputStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            inputStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: inputStackView.topAnchor)
        ])
    }
    
    
    @objc func keyboardWillShow(notification: Notification){
        guard let keyboardFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else { return }
        
        let keyboardHeight = keyboardFrame.height
        
        UIView.animate(withDuration: 0.3) {
            self.view.frame.origin.y = -keyboardHeight
        }
    }
    
    @objc func keyboardWillHide(){
        UIView.animate(withDuration: 0.3) {
            self.view.frame.origin.y = 0
        }
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }


}

extension MessengerViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let message = messages[indexPath.row]
        
        if message.sender == "A-Lin" {
            guard let senderCell = tableView.dequeueReusableCell(withIdentifier: "\(SenderTableViewCell.self)", for: indexPath) as? SenderTableViewCell else { fatalError("") }

            senderCell.senderImageView.image = UIImage(named: "alin")
            senderCell.senderTextView.text = message.messageContent
            senderCell.senderNameLabel.text = message.sender
            senderCell.senderTextView.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
            senderCell.senderTextView.layer.cornerRadius = 10
            senderCell.senderImageView.layer.cornerRadius = 24
            senderCell.selectionStyle = .none

            return senderCell
            
        } else {
            
            guard let receiverCell = tableView.dequeueReusableCell(withIdentifier: "\(ReceiverTableViewCell.self)", for: indexPath) as? ReceiverTableViewCell else { fatalError("") }
            receiverCell.receiverTextView.text = message.messageContent
            receiverCell.selectionStyle = .none
            return receiverCell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         view.endEditing(true)
    }


}


extension MessengerViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        inputTextField.resignFirstResponder()
        return true
    }
}
