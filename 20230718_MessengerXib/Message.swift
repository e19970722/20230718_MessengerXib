//
//  Message.swift
//  20230718_MessengerXib
//
//  Created by Yen Lin on 2023/7/18.
//

import Foundation

// 09:38

struct Message: Codable {
    var sender: String
    var messageContent: String
    
    static let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    static func saveMessage(_ messages: [Self]) {
        let encoder = JSONEncoder()
        let data = try? encoder.encode(messages)
        let url = documentsDirectory.appendingPathComponent("message")
        
        try? data?.write(to: url)
    }
    
    static func loadMessage() -> [Message]? {
        let decoder = JSONDecoder()
        let url = documentsDirectory.appendingPathComponent("message")
        guard let data = try? Data(contentsOf: url) else { return nil }
        return try? decoder.decode([Message].self, from: data)
    }
    
    
}
