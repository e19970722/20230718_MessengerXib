//
//  RecieverTableViewCell.swift
//  20230718_MessengerXib
//
//  Created by Yen Lin on 2023/7/18.
//

import UIKit

class ReceiverTableViewCell: UITableViewCell {

    var receiverTextView: UITextView = {
        let textView = UITextView()
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.backgroundColor = .link
        textView.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        textView.layer.cornerRadius = 10
        textView.textColor = .white
        textView.font = .systemFont(ofSize: 14)
        return textView
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "\(ReceiverTableViewCell.self)")
        
        contentView.addSubview(receiverTextView)
        receiverTextView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            receiverTextView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            receiverTextView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
            receiverTextView.leadingAnchor.constraint(greaterThanOrEqualTo: contentView.leadingAnchor, constant: 80),
            receiverTextView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -16)
        ])
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    
}
