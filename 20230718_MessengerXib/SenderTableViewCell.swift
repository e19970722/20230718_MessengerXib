//
//  MessengerTableViewCell.swift
//  20230718_MessengerXib
//
//  Created by Yen Lin on 2023/7/18.
//

import UIKit

class SenderTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var senderImageView: UIImageView!
    @IBOutlet weak var senderTextView: UITextView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
